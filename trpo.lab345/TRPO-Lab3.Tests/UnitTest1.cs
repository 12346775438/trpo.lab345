using NUnit.Framework;
using TRPO_Lab.Lib;

namespace TrpoLab3.Tests
{
    [TestFixture]
    public class CalcTests
    {
        RightPolygon RightPolygon = new RightPolygon();
        [Test]
        public void AddTest()
        {
           
            const double a = 4;
            const int b = 4;
            const int expected = 16;
            RightPolygon.Square(a, b);
            double result = RightPolygon.result;
            Assert.AreEqual(expected, Math.Round(result, 3));
        }

        [Test]
        public void NegativeVal()
        {

            const double a = -4;
            const int b = 4;
            RightPolygon.Square(a, b);
            double result = RightPolygon.result;
            Assert.IsFalse(RightPolygon.result < 0, "������������� ��������");
        }
    }
}
