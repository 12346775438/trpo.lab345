﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TRPO_Lab.Lib;


namespace trpo.lab345
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                RightPolygon RightPolygon = new RightPolygon();
                Console.Write("Введите длину стороны a: ");
                double _a = Convert.ToDouble(Console.ReadLine());
                Console.Write("Введите количество сторон n: ");
                int _n = Convert.ToInt16(Console.ReadLine());
                if (_n < 3 || _a < 0)
                {
                    Console.WriteLine("Неверное количество сторон или длина \n------");
                }
                else
                {
                    RightPolygon.Square(_a, _n);
                    double _result = RightPolygon.result;
                    Console.WriteLine("Площадь правильного многоугольнка равна: " + Math.Round(_result, 3) + "\n-----");
                }
            }
        }
    }
}