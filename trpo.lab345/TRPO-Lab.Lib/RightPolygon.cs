﻿namespace TRPO_Lab.Lib
{
    public class RightPolygon
    {
        public double result;
        public void Square(double a, int n)
        {
             result = n * Math.Pow(a, 2) / (4 * Math.Tan(180 * (Math.PI / 180) / n));

        }
    }

}