﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Shapes;
using TRPO_Lab.Lib;

namespace TRPO_Lab4.WpfApp
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private double _a;
        private int _n;
        private double _result;
        private string _errorMessage;
        private readonly RightPolygon _RightPolygon;

        public double a
        {
            get => _a;
            set
            {
                if (value <= 0)
                {
                    ErrorMessage = "Длина стороны должна быть больше 0";
                    return;
                }
                else
                {
                    _a = value;
                    Square();
                    OnPropertyChanged();
                }

            }
        }

        public int n
        {
            get => _n;
            set
            {
                if (value <= 2)
                {
                    ErrorMessage = "Количество сторон должно быть 3 и больше";
                    return;
                }
                else
                {
                    _n = value;
                    Square();
                    OnPropertyChanged();
                }

            }
        }

        public double result
        {
            get => _result;
            set
            {
                _result = value;
                OnPropertyChanged();
            }
        }

        public string ErrorMessage
        {
            get => _errorMessage;
            set
            {
                _errorMessage = value;
                OnPropertyChanged();
            }
        }

        public MainViewModel()
        {
            _RightPolygon = new RightPolygon();
        }

        private void Square()
        {
            _RightPolygon.Square(a, n);
           result = Math.Round(_RightPolygon.result, 3);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
